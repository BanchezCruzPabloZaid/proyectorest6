<?php if(!defined("SPECIALCONSTANT")) die ("Acceso denegado");

 $app->container->singleton('db', function () {
		$user = "administrador";
		$password = "abd";
		$dbname = "poblacionyvivienda";
		$port = "5432";
		$host = "localhost";
		return new PDO("pgsql:host=$host; dbname=$dbname", $user, $password);
});

function id_invalido($id)
{
	return is_numeric($id);
}

function cadena_invalida($cadena)
{
	return is_string($cadena);
}

$app->get("/temas", function() use($app)
{
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("SELECT * FROM tema1");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->response->status(200);
			$app->render('xml.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("SELECT * FROM tema1");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->response->status(200);
			$app->render('json.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
	
});	

$app->get("/temas/:id", function($id) use($app)
{
		if(id_invalido($id)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("SELECT * FROM tema1 where id_tema1=$id");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->response->status(200);
			$app->render('xml.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("SELECT * FROM tema1 where id_tema1=$id");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->response->status(200);
			$app->render('json.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
	
});

$app->get("/temas/:id/subtemas", function($id) use($app)
{
		if(id_invalido($id)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("select * from tema2 where id_tema1=$id");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->response->status(200);
			$app->render('xmlid.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("select * from tema2 where id_tema1=$id");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->response->status(200);
			$app->render('jsonid.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
	
});

$app->get("/temas/:id/subtemas/:id2", function($id,$id2) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("select * from tema2 where id_tema1=$id and id_tema2=$id2");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->response->status(200);
			$app->render('xmlid.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("select * from tema2 where id_tema1=$id and id_tema2=$id2");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->response->status(200);
			$app->render('jsonid.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
	
});

$app->get("/temas/:id/subtemas/:id2/subtemas", function($id,$id2) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("select * from tema3 inner join tema2 on tema3.id_tema2=tema2.id_tema2 where tema2.id_tema1=$id and tema2.id_tema2=$id2");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->response->status(200);
			$app->render('xmlid2.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("select * from tema3 inner join tema2 on tema3.id_tema2=tema2.id_tema2 where tema2.id_tema1=$id and tema2.id_tema2=$id2");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->response->status(200);
			$app->render('jsonid2.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
	
});

$app->get("/temas/:id/subtemas/:id2/subtemas/:id3", function($id,$id2,$id3) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false||id_invalido($id3)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("select * from tema3 inner join tema2 on tema3.id_tema2=tema2.id_tema2 where tema2.id_tema1=$id and tema2.id_tema2=$id2 and tema3.id_tema3=$id3");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->response->status(200);
			$app->render('xmlid2.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("select * from tema3 inner join tema2 on tema3.id_tema2=tema2.id_tema2 where tema2.id_tema1=$id and tema2.id_tema2=$id2 and tema3.id_tema3=$id3");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->response->status(200);
			$app->render('jsonid2.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
	
});

$app->get("/temas/:id/subtemas/:id2/subtemas/:id3/indicadores", function($id,$id2,$id3) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false||id_invalido($id3)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("select * from censo inner join indicadores on  censo.id_indicador=indicadores.id_indicador where censo.id_t3=$id3");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->response->status(200);
			$app->render('xmli.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("select * from censo inner join indicadores on  censo.id_indicador=indicadores.id_indicador where censo.id_t3=$id3");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->response->status(200);
			$app->render('jsoni.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
	
});

$app->get("/temas/:id/subtemas/:id2/subtemas/:id3/indicadores/:id4", function($id,$id2,$id3,$id4) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false||id_invalido($id3)===false||id_invalido($id4)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("select * from censo inner join indicadores on  censo.id_indicador=indicadores.id_indicador where censo.id_t3=$id3 and censo.id_indicador=$id4");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->response->status(200);
			$app->render('xmli.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("select * from censo inner join indicadores on  censo.id_indicador=indicadores.id_indicador where censo.id_t3=$id3 and censo.id_indicador=$id4");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->response->status(200);
			$app->render('jsoni.php', array('resultados' => $consulta->fetchAll(PDO::FETCH_ASSOC)));
		}
	
});

$app->post("/temas/", function() use($app)
{
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$body=$app->request->getBody();
			$temaxml=DOMDocument::loadXML($body);
			$nodos_tema=$temaxml->getElementsByTagName('descripcion');
			$nodo_tema=$nodos_tema->item(0);
			$tema=$nodo_tema->nodeValue;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion $tema es vacio");
			}
			$consulta=$app->db->prepare("insert into tema1 values(5,'$tema')");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->halt(201,"Se registro el nuevo tema $tema");
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$body=$app->request->getBody();
			$tema=json_decode($body)->tema1->descripcion;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("insert into tema1 values(16,'$tema')");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->halt(201,"Se registro el nuevo tema $tema");
		}
});	

$app->post("/temas/:id/subtemas", function($id) use($app)
{
		if(id_invalido($id)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$body=$app->request->getBody();
			$temaxml=DOMDocument::loadXML($body);
			$nodos_tema=$temaxml->getElementsByTagName('descripcion');
			$nodo_tema=$nodos_tema->item(0);
			$tema=$nodo_tema->nodeValue;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("insert into tema2 values(73,$id,'$tema')");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			$app->response->headers->set("Content-type","application/xml");
			$mensaje = '<?xml version="1.0" encoding="UTF-8"?>
							<mensaje>mensaje
								<descripcion> se creo el nuevo tema</descripcion>	
							</mensaje>';
			$app->halt(201,$mensaje);
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$body=$app->request->getBody();
			$tema=json_decode($body)->tema2->descripcion;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("insert into tema2 values(22,$id,'$tema')");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			$app->response->headers->set("Content-type","application/json");
			$app->halt(201,"Se registro el nuevo tema $tema");
		}
});

$app->post("/temas/:id/subtemas/:id2/subtemas", function($id,$id2) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$body=$app->request->getBody();
			$temaxml=DOMDocument::loadXML($body);
			$nodos_tema=$temaxml->getElementsByTagName('descripcion');
			$nodo_tema=$nodos_tema->item(0);
			$tema=$nodo_tema->nodeValue;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("insert into tema3 values(23,$id2,'$tema')");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->halt(201,"Se registro el nuevo tema $tema");
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$body=$app->request->getBody();
			$tema=json_decode($body)->tema3->descripcion;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("insert into tema3 values(234,$id2,'$tema')");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			$app->response->headers->set("Content-type","application/json");
			$app->halt(201,"Se registro el nuevo tema $tema");
		}
});

$app->post("/temas/:id/subtemas/:id2/subtemas/:id3/indicadores", function($id,$id2,$id3) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false||id_invalido($id3)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$body=$app->request->getBody();
			$indicadorxml=DOMDocument::loadXML($body);
			$nodos_indicador=$indicadorxml->getElementsByTagName('descripcion');
			$nodo_indicador=$nodos_indicador->item(0);
			$indicador=$nodo_indicador->nodeValue;
			if(cadena_invalida($indicador)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$nodos_indicador=$indicadorxml->getElementsByTagName('anio');
			$nodo_indicador=$nodos_indicador->item(0);
			$anio=$nodo_indicador->nodeValue;
			if(cadena_invalida($anio)===false)
			{
				$app->halt(400, "El campo anio es vacio");
			}
			$consulta=$app->db->prepare("insert into indicadores values(34,'$indicador','sjsjsj')");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$indicador);
			}
			$consulta2=$app->db->prepare("insert into censo values(34,1,$id,$id2,$id3,$anio)");
			if ($consulta2->execute() === false)
			{
				$app->halt(500,$indicador);
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->halt(201,"Se registro el nuevo tema $indicador");
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$body=$app->request->getBody();
			$indicador=json_decode($body)->indicador->descripcion;
			if(cadena_invalida($indicador)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$anio=json_decode($body)->indicador->anio;
			if(cadena_invalida($anio)===false)
			{
				$app->halt(400, "El campo anio es vacio");
			}
			$consulta=$app->db->prepare("insert into indicadores values(45,'$indicador','sjsjsj')");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$indicador);
			}
			$consulta2=$app->db->prepare("insert into censo values(45,1,$id,$id2,$id3,$anio)");
			if ($consulta2->execute() === false)
			{
				$app->halt(500,$indicador);
			}
			$app->response->headers->set("Content-type","application/json");
			$app->halt(201,"Se registro el nuevo tema $indicador");
		}
});

$app->delete("/temas/:id", function($id) use($app)
{
		if(id_invalido($id)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("delete from tema1 where id_tema1=$id");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->halt(200,"se borro el tema con el id '$id'");
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("delete tema1 where id_tema1=$id");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->halt(200,"se borro el tema con el id '$id'");
		}
	
});

$app->delete("/temas/:id/subtemas/:id2", function($id,$id2) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("delete from tema2 where id_tema2=$id2");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->halt(200,"se borro el tema con el id '$id2'");
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("delete from tema2 where id_tema2=$id2");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->halt(200,"se borro el tema con el id '$id2'");
		}
	
});

$app->delete("/temas/:id/subtemas/:id2/subtemas/:id3", function($id,$id2,$id3) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false||id_invalido($id3)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("delete from tema3 where id_tema3=$id3");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->halt(200,"se borro el tema con el id '$id3'");
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("delete from tema3 where id_tema3=$id3");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->halt(200,"se borro el tema con el id '$id3'");
		}
	
});

$app->delete("/temas/:id/subtemas/:id2/subtemas/:id3/indicadores/:id4", function($id,$id2,$id3,$id4) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false||id_invalido($id3)===false||id_invalido($id4)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$consulta=$app->db->prepare("delete from censo where id_indicador=$id4");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$consulta=$app->db->prepare("delete from  indicadores where id_indicador=$id4");
			$app->response->headers->set("Content-type","application/xml");
			$mensaje = '<?xml version="1.0" encoding="UTF-8"?>
							<mensaje>mensaje
								<descripcion>se borro el indicador con el id '$id4'</descripcion>	
							</mensaje>';
			$app->halt(200,$mensaje);
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$consulta=$app->db->prepare("delete from censo where id_indicador=$id4");
			if ($consulta->execute() === false)
			{
				$app->halt(500,"Error en el servidor");
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$consulta=$app->db->prepare("delete from  indicadores where id_indicador=$id4");
			$app->response->headers->set("Content-type","application/json");
			$app->halt(200,"se borro el indicador con el id '$id4'");
		}
	
});

$app->put("/temas/:id", function($id) use($app)
{
		if(id_invalido($id)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$body=$app->request->getBody();
			$temaxml=DOMDocument::loadXML($body);
			$nodos_tema=$temaxml->getElementsByTagName('descripcion');
			$nodo_tema=$nodos_tema->item(0);
			$tema=$nodo_tema->nodeValue;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("update tema1 set desc_tema1='$tema' where id_tema1=$id");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$mensaje = '<?xml version="1.0" encoding="UTF-8"?>
							<mensaje>mensaje
								<descripcion> se borro el registro $id</descripcion>	
							</mensaje>';
			$app->halt(201,$mensaje);
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$body=$app->request->getBody();
			$tema=json_decode($body)->tema1->descripcion;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("update tema1 set desc_tema1='$tema' where id_tema1=$id");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->halt(201,"Se registro el nuevo tema $tema");
		}
});	

$app->put("/temas/:id/subtemas/:id2", function($id,$id2) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$body=$app->request->getBody();
			$temaxml=DOMDocument::loadXML($body);
			$nodos_tema=$temaxml->getElementsByTagName('descripcion');
			$nodo_tema=$nodos_tema->item(0);
			$tema=$nodo_tema->nodeValue;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("update tema2 set desc_tema2='$tema' where id_tema2=$id2");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->halt(201,"Se registro el nuevo tema $tema");
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$body=$app->request->getBody();
			$tema=json_decode($body)->tema2->descripcion;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("update tema2 set desc_tema2='$tema' where id_tema2=$id2");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->halt(201,"Se registro el nuevo tema $tema");
		}
});

$app->put("/temas/:id/subtemas/:id2/subtemas/:id3", function($id,$id2,$id3) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false||id_invalido($id3)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$body=$app->request->getBody();
			$temaxml=DOMDocument::loadXML($body);
			$nodos_tema=$temaxml->getElementsByTagName('descripcion');
			$nodo_tema=$nodos_tema->item(0);
			$tema=$nodo_tema->nodeValue;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("update tema3 set desc_tema3='$tema' where id_tema3=$id3");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->halt(201,"Se registro el nuevo tema $tema");
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$body=$app->request->getBody();
			$tema=json_decode($body)->tema3->descripcion;
			if(cadena_invalida($tema)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("update tema3 set desc_tema3='$tema' where id_tema3=$id3");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$tema);
			}
			if ($consulta->rowCount() == 0) 
			{
				$app->halt(404, "Error: El indicador con clave '$id' no existe.");
			}
			$app->response->headers->set("Content-type","application/json");
			$app->halt(201,"Se registro el nuevo tema $tema");
		}
});

$app->put("/temas/:id/subtemas/:id2/subtemas/:id3/indicadores/:id4", function($id,$id2,$id3,$id4) use($app)
{
		if(id_invalido($id)===false||id_invalido($id2)===false||id_invalido($id3)===false||id_invalido($id4)===false)
		{
			$app->halt(400, "Error: en el id '$id' no cumple con el formato correcto");
		}
		if($app->request->headers->get('ACCEPT')==='application/xml')
		{
			$body=$app->request->getBody();
			$indicadorxml=DOMDocument::loadXML($body);
			$nodos_indicador=$indicadorxml->getElementsByTagName('descripcion');
			$nodo_indicador=$nodos_indicador->item(0);
			$indicador=$nodo_indicador->nodeValue;
			if(cadena_invalida($indicador)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("update indicadores set descripcion='$indicador' where id_indicador=$id4");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$indicador);
			}
			$app->response->headers->set("Content-type","application/xml");
			$app->halt(201,"Se registro el nuevo tema $indicador");
		}
		if($app->request->headers->get('ACCEPT')==='application/json')
		{
			$body=$app->request->getBody();
			$indicador=json_decode($body)->indicador->descripcion;
			if(cadena_invalida($indicador)===false)
			{
				$app->halt(400, "El campo descripcion es vacio");
			}
			$consulta=$app->db->prepare("update indicadores set descripcion='$indicador' where id_indicador=$id4");
			if ($consulta->execute() === false)
			{
				$app->halt(500,$indicador);
			}
			$app->response->headers->set("Content-type","application/json");
			$app->halt(201,"Se registro el nuevo tema $indicador");
		}
});